package com.igmi.om.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SigninController {

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String signin(ModelMap model) {
        return "signin";
    }

    @RequestMapping(value = "/signinError", method = RequestMethod.GET)
    public String signinFailure(ModelMap model) {
        model.addAttribute("message", "Failure!");
        return "signin";
    }
}
