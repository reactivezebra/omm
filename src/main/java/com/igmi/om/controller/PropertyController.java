package com.igmi.om.controller;

import com.igmi.om.form.PropertyTemplate;
import com.igmi.om.repository.PropertyTemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PropertyController {

    @Autowired
    private PropertyTemplateRepository propertyFormRepository;

    @RequestMapping(value = "/property/createproperty", method = RequestMethod.POST)
    public String savePropertyScheme(@ModelAttribute("paramsTemplate") PropertyTemplate form, BindingResult result) {
        propertyFormRepository.save(form);
        return "redirect:/template/properties";
    }

    @RequestMapping(value = "/property/addproptemplate")
    public String getPropertySchemePage(ModelMap modelMap) {
        return "template/addparamstemplate";
    }

    @RequestMapping(value = "/template/properties", method = RequestMethod.GET)
    public String listProperties(ModelMap modelMap) {
        modelMap.addAttribute("properties", propertyFormRepository.findAll());

        return "template/properties";
    }

    @RequestMapping(value="/template/property/{propId}", method = RequestMethod.GET)
    public String getProperty(@PathVariable("propId") Long propId, ModelMap model){
        PropertyTemplate form = propertyFormRepository.findOne(propId);
        model.addAttribute("propertyForm", form);

        return "/template/property";
    }

}
