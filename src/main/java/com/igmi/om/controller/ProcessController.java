package com.igmi.om.controller;

import com.igmi.om.domain.process.Process;
import com.igmi.om.domain.task.Task;
import com.igmi.om.form.ProcessForm;
import com.igmi.om.repository.ProcessRepository;
import com.igmi.om.repository.TaskRepository;
import com.igmi.om.service.entity.EntityFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ProcessController {

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private EntityFactory entityFactory;


    @RequestMapping(value = "/processes", method = RequestMethod.GET)
    public String listProcesses(ModelMap modelMap) {
        modelMap.addAttribute("processes", processRepository.findAll());
        return "process/processes";
    }

    @RequestMapping(value = "/process/add", method = RequestMethod.GET)
    public String getProcessForm(ModelMap modelMap) {
        modelMap.addAttribute("processForm", new ProcessForm());
        List<Task> taskList = taskRepository.findAllTaskTemplates();
        modelMap.addAttribute("taskList", taskList);
        return "process/addprocess";
    }

    @RequestMapping(value = "/process/add", method = RequestMethod.POST)
    public String addProcess(@ModelAttribute("processForm") ProcessForm processForm, BindingResult result) {
        System.out.println("processForm = " + processForm);
        System.out.println(processForm.getTasks());
        Process process = entityFactory.extractProcess(processForm);
        processRepository.save(process);
        return "redirect:/processes";
    }

    @RequestMapping(value = "/process/{processId}", method = RequestMethod.GET)
    public String showProcess(@PathVariable("processId") Long processId, ModelMap model) {
        Process process = processRepository.findOne(processId);
        model.addAttribute("process", process);
        return "process/process";
    }

    @RequestMapping(value = "/process/edit/{processId}", method = RequestMethod.POST)
    public String editProcess(@PathVariable("processId") Long processId, @ModelAttribute("process") Process process, BindingResult result) {

        return "redirect:/process/" + processId;
    }


}
