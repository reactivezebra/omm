package com.igmi.om.controller;

import com.igmi.om.domain.instance.Instance;
import com.igmi.om.repository.InstanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class InstanceController {

    @Autowired
    private InstanceRepository instanceRepository;

    @RequestMapping(value = "/instances", method = RequestMethod.GET)
    public String listInstances(ModelMap modelMap) {
        modelMap.addAttribute("instances", instanceRepository.findAll());
        return "instance/instances";
    }

    @RequestMapping(value = "/instance/add", method = RequestMethod.GET)
    public String getInstanceForm(ModelMap modelMap) {
        modelMap.addAttribute("instance", new Instance());
        return "instance/addinstance";
    }

    @RequestMapping(value = "/instance/add", method = RequestMethod.POST)
    public String addInstance(@ModelAttribute("instance") Instance instance, BindingResult result) {
        instanceRepository.save(instance);
        return "redirect:/instances";
    }

    @RequestMapping(value = "/instance/{instanceId}", method = RequestMethod.GET)
    public String showInstance(@PathVariable("instanceId") Long instanceId, ModelMap model) {

        return "instance/instance";
    }

    @RequestMapping(value = "/instance/edit/{instanceId}", method = RequestMethod.POST)
    public String editInstance(@PathVariable("instanceId") Long instanceId, @ModelAttribute("instance") Instance instance, BindingResult result) {

        return "redirect:/instance/" + instanceId;
    }
}
