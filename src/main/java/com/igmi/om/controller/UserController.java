package com.igmi.om.controller;

import com.igmi.om.domain.User;
import com.igmi.om.repository.UserRepository;
import com.igmi.om.service.authorization.PasswordEncoderProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoderProvider passwordEncoderProvider;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String listUsers(ModelMap model) {
        model.addAttribute("users", userRepository.findAll());
        return "user/users";
    }

    @RequestMapping(value = "/user/add", method = RequestMethod.GET)
    public String getUserForm(ModelMap modelMap) {
        modelMap.addAttribute("user", new User());
        return "user/adduser";
    }

    @RequestMapping(value = "/user/add", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("user") User user, BindingResult result) {
        user.setPassword(passwordEncoderProvider.encode(user.getPassword()));
        if (user.getIsAdmin())
            user.setRoles("ROLE_USER, ROLE_ADMIN");
        else
            user.setRoles("ROLE_USER");
        userRepository.save(user);

        return "redirect:/users";
    }

    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public String showUser(@PathVariable("userId") Long userId, ModelMap model) {
        User user = userRepository.findOne(userId);
        model.addAttribute("user", user);
        return "user/user";
    }

    @RequestMapping(value = "/user/edit/{userId}", method = RequestMethod.POST)
    public String editUser(@PathVariable("userId") Long userId, @ModelAttribute("user") User user, BindingResult result) {
        User sourceUser = userRepository.findOne(userId);
        sourceUser.update(user);
        userRepository.save(sourceUser);
        return "redirect:/user/" + userId;
    }

    @RequestMapping("/delete/{userId}")
    public String deleteUser(@PathVariable("userId") Long userId) {

        userRepository.delete(userRepository.findOne(userId));

        return "redirect:/users";
    }
}
