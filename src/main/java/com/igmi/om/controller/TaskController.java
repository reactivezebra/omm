package com.igmi.om.controller;

import com.igmi.om.domain.task.AutoTask;
import com.igmi.om.domain.task.ManualTask;
import com.igmi.om.domain.task.Task;
import com.igmi.om.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class TaskController {
    @Autowired
    private TaskRepository taskRepository;

    @RequestMapping(value = "/tasks", method = RequestMethod.GET)
    public String listTasks(ModelMap modelMap) {
        modelMap.addAttribute("tasks", taskRepository.findAll());
        return "task/tasks";
    }

    @RequestMapping(value = "/task/add/auto", method = RequestMethod.GET)
    public String showAutoTaskForm(ModelMap modelMap) {
        modelMap.addAttribute("task", new AutoTask());
        return "task/addautotask";
    }

    @RequestMapping(value = "/task/add/auto", method = RequestMethod.POST)
    public String addAutoTask(@ModelAttribute("task") AutoTask task, BindingResult result) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/task/add/manual", method = RequestMethod.GET)
    public String showManualTaskForm(ModelMap modelMap) {
        modelMap.addAttribute("task", new ManualTask());
        return "task/addmanualtask";
    }

    @RequestMapping(value = "/task/add/manual", method = RequestMethod.POST)
    public String addManualTask(@ModelAttribute("task") ManualTask task, BindingResult result) {
        taskRepository.save(task);
        return "redirect:/tasks";
    }

    @RequestMapping(value = "/task/{taskId}", method = RequestMethod.GET)
    public String showTask(@PathVariable("taskId") Long taskId, ModelMap model) {
        Task task = taskRepository.findOne(taskId);
        if (task instanceof AutoTask) {
            model.addAttribute("task", task);
            return "task/autotask";
        }
        if (task instanceof ManualTask) {
            model.addAttribute("task", task);
            return "task/manualtask";
        }
        return "error404";
    }

    @RequestMapping(value = "/task/edit/auto/{taskId}", method = RequestMethod.POST)
    public String editAutoTask(@PathVariable("taskId") Long taskId, @ModelAttribute("task") AutoTask task, BindingResult result) {
        Task sourceTask = taskRepository.findOne(taskId);
        sourceTask.update(task);
        taskRepository.save(sourceTask);
        return "redirect:/task/" + taskId;
    }

    @RequestMapping(value = "/task/edit/manual/{taskId}", method = RequestMethod.POST)
    public String editManualTask(@PathVariable("taskId") Long taskId, @ModelAttribute("task") ManualTask task, BindingResult result) {
        Task sourceTask = taskRepository.findOne(taskId);
        sourceTask.update(task);
        taskRepository.save(sourceTask);
        return "redirect:/task/" + taskId;
    }

}
