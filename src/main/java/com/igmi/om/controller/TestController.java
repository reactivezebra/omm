package com.igmi.om.controller;

import com.igmi.om.action.TestAction;
import com.igmi.om.common.PropertyScheme;
import com.igmi.om.common.PropertyType;
import com.igmi.om.common.Status;
import com.igmi.om.domain.User;
import com.igmi.om.domain.instance.Instance;
import com.igmi.om.domain.order.Order;
import com.igmi.om.domain.process.Process;
import com.igmi.om.domain.task.AutoTask;
import com.igmi.om.domain.task.ManualTask;
import com.igmi.om.domain.task.Task;
import com.igmi.om.form.PropertyTemplate;
import com.igmi.om.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

@Controller
public class TestController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private InstanceRepository instanceRepository;

    @Autowired
    private PropertyTemplateRepository propertyTemplateRepository;

    @Autowired
    private TaskRepository taskRepository;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(ModelMap model) {
        User user = userRepository.findUserByLogin("igmi");
        if (user != null)
            model.addAttribute("message", user);
        User user1 = new User("igor", "igor", "ROLE_USER");
        userRepository.save(user1);
        user1.setIsAdmin(true);
        userRepository.save(user1);
        return "test/test";
    }

    @RequestMapping(value = "/test/order", method = RequestMethod.GET)
    public String testOrder(ModelMap model) {
        Order order = new Order();
        System.out.println("Order=" + order.getId());
        orderRepository.save(order);

        PropertyScheme scheme = new PropertyScheme();
        scheme.setPropertyName("test1");
        scheme.setPropertyType(PropertyType.NUMBER);

        PropertyScheme scheme1 = new PropertyScheme();
        scheme1.setPropertyName("test2");
        scheme1.setPropertyType(PropertyType.STRING);

        PropertyTemplate template = new PropertyTemplate();
        template.setSchemeName("ololo");
        template.setSchemeDescription("ASFASF");
        template.addProperty(scheme);
        template.addProperty(scheme1);

        order.setTemplate(template);
        orderRepository.save(order);
        return "test/testorder";
    }

    @RequestMapping(value = "/test/process", method = RequestMethod.GET)
    public String testProcess(ModelMap model) {
        Process process = new Process();
        Task task = new ManualTask();
        process.addTask(task);
        Task task1 = new AutoTask();
        process.addTask(task1);
        process.setStatus(Status.Completed);
        processRepository.save(process);
        return "test/testprocess";
    }

    @RequestMapping(value = "/test/instance", method = RequestMethod.GET)
    public String testInstance(ModelMap model) {
        Order order = new Order();
        Process process = new Process();
        Task task = new ManualTask();
        process.addTask(task);
        Task task1 = new AutoTask();
        process.addTask(task1);

        order.setProcess(process);

        Instance instance = new Instance();
        order.setInstance(instance);
        orderRepository.save(order);

        return "test/testprocess";
    }

    @RequestMapping(value = "/test/all", method = RequestMethod.GET)
    public String testAll(ModelMap model) {
        Order order = new Order();
        orderRepository.save(order);
        Process process = new Process();
        processRepository.save(process);
        Task task = new ManualTask();
        process.addTask(task);
        Task task1 = new AutoTask();
        process.addTask(task1);
        processRepository.save(process);

        order.setProcess(process);

        Instance instance = new Instance();
        order.setInstance(instance);
        instanceRepository.save(instance);
        orderRepository.save(order);

        return "test/testprocess";
    }

    @RequestMapping(value = "/test/invoke", method = RequestMethod.GET)
    public String invoke(ModelMap model) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {

        User user = new User();
        Class<?> c = Class.forName("com.igmi.om.action.TestAction");
        Method method = c.getDeclaredMethod("execute", User.class);
        method.invoke(new TestAction(), user);
        model.addAttribute("user", user);

        return "test/testprocess";
    }

    @RequestMapping(value = "/test/ord", method = RequestMethod.GET)
    public String addButton(ModelMap model) {
        List<Task> tasks = taskRepository.findAllTaskTemplates();
        System.out.println("tasks size=" + tasks.size());
        return "template/addparamstemplate";
    }
}