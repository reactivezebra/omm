package com.igmi.om.controller;

import com.igmi.om.domain.order.Order;
import com.igmi.om.form.OrderForm;
import com.igmi.om.form.PropertyTemplate;
import com.igmi.om.repository.OrderRepository;
import com.igmi.om.repository.PropertyTemplateRepository;
import com.igmi.om.service.entity.EntityFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class OrderController {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private PropertyTemplateRepository propertyTemplateRepository;

    @Autowired
    private EntityFactory entityFactory;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String listOrders(ModelMap modelMap) {
        List<Order> orders = orderRepository.findAll();
        modelMap.addAttribute("orders", orders);
        return "order/orders";
    }

    @RequestMapping(value = "/order/add", method = RequestMethod.GET)
    public String getOrderForm(ModelMap modelMap) {
        modelMap.addAttribute("orderForm", new OrderForm());
        List<PropertyTemplate> templateList = propertyTemplateRepository.findAll();
        modelMap.addAttribute("templateList", templateList);
        return "order/addorder";
    }

    @RequestMapping(value = "/order/add", method = RequestMethod.POST)
    public String addOrder(@ModelAttribute("orderForm") OrderForm orderForm, BindingResult result) {
        Order order = entityFactory.extractOrder(orderForm);
        orderRepository.save(order);
        return "redirect:/orders";
    }

    @RequestMapping(value = "/order/{orderId}", method = RequestMethod.GET)
    public String showOrder(@PathVariable("orderId") Long orderId, ModelMap model) {
        Order order = orderRepository.findOne(orderId);
        model.addAttribute("order", order);
        return "order/order";
    }

    @RequestMapping(value = "/order/edit/{orderId}", method = RequestMethod.POST)
    public String editOrder(@PathVariable("orderId") Long orderId, @ModelAttribute("order") Order order, BindingResult result) {
        Order sourceOrder = orderRepository.findOne(orderId);
        sourceOrder.update(order);
        orderRepository.save(sourceOrder);
        return "redirect:/order/" + orderId;
    }

}
