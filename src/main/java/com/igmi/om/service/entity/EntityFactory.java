package com.igmi.om.service.entity;

import com.igmi.om.common.TaskScheme;
import com.igmi.om.domain.order.Order;
import com.igmi.om.domain.process.Process;
import com.igmi.om.domain.task.Task;
import com.igmi.om.form.OrderForm;
import com.igmi.om.form.ProcessForm;
import com.igmi.om.repository.PropertyTemplateRepository;
import com.igmi.om.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EntityFactory {

    @Autowired
    private PropertyTemplateRepository propertyTemplateRepository;

    @Autowired
    private TaskRepository taskRepository;

    public EntityFactory() {
    }

    public Order extractOrder(OrderForm form) {
        Order order = new Order();
        order.setName(form.getName());
        order.setDescription(form.getDescription());
        order.setCustomer(form.getCustomer());
        order.setStartDate(form.getStartDate());
        order.setEndDate(form.getExpectedEndDate());
        order.setOrderAim(form.getOrderAim());

        System.out.println(propertyTemplateRepository.findOne(form.getTemplateId()));

        order.setTemplate(propertyTemplateRepository.findOne(form.getTemplateId()));
        return order;
    }

    public Process extractProcess(ProcessForm form) {
        Process process = new Process();
        process.setName(form.getName());
        System.out.println("tasks");
        for (TaskScheme taskScheme : form.getTasks()) {
            System.out.printf("taskscheme=" + taskScheme.getTaskId() + " indec=" + taskScheme.getIndex());
            Task task = taskRepository.findOne(taskScheme.getTaskId());
            Task concreteTask = task.clone();
            concreteTask.setIndex(taskScheme.getIndex());
            process.addTask(concreteTask);
        }
        return process;
    }
}
