package com.igmi.om.service.authorization;

import org.springframework.security.crypto.password.PasswordEncoder;

public class PasswordEncoderProvider {

    private PasswordEncoder encoder;

    public PasswordEncoderProvider(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    public PasswordEncoderProvider() {
    }

    public String encode(String password){
        return encoder.encode(password);
    }
}
