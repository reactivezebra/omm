package com.igmi.om.service.orchestrator;

import com.igmi.om.action.TestAction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AutoTaskInvoker {

    public void execute(String actionName) {
        try {
            Class<?> c = Class.forName(actionName);
            Method method = c.getDeclaredMethod("execute");
            method.invoke(new TestAction());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
