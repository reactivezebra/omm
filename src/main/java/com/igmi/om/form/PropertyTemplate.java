package com.igmi.om.form;

import com.igmi.om.common.PropertyScheme;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "property_template")
public class PropertyTemplate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String schemeName;
    private String schemeDescription;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<PropertyScheme> items = new ArrayList<PropertyScheme>();

    public List<PropertyScheme> getItems() {
        return items;
    }

    public void setItems(List<PropertyScheme> items) {
        this.items = items;
    }

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void addProperty(PropertyScheme propertyScheme) {
        items.add(propertyScheme);
    }
}
