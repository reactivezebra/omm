package com.igmi.om.form;


import com.igmi.om.common.TaskScheme;

import java.util.ArrayList;
import java.util.List;

public class ProcessForm {

    private String name;
    private List<TaskScheme> tasks = new ArrayList<TaskScheme>();

    public ProcessForm(String name, List<TaskScheme> tasks) {
        this.name = name;
        this.tasks = tasks;
    }

    public ProcessForm() {
    }

    public List<TaskScheme> getTasks() {
        return tasks;
    }

    public void setTasks(List<TaskScheme> tasks) {
        this.tasks = tasks;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
