package com.igmi.om.form;

import com.igmi.om.domain.order.Order;

public class OrderForm extends Order {

    private Long templateId;

    public Long getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Long templateId) {
        this.templateId = templateId;
    }
}
