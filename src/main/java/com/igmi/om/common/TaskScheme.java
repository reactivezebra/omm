package com.igmi.om.common;

public class TaskScheme {

    private Long taskId;
    private Long index;

    public TaskScheme(Long taskId, Long index) {
        this.taskId = taskId;
        this.index = index;
    }

    public TaskScheme() {
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public Long getIndex() {
        return index;
    }

    public void setIndex(Long index) {
        this.index = index;
    }
}
