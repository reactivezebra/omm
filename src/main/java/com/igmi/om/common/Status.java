package com.igmi.om.common;

public enum Status {

    Planned,
    InProgress,
    Completed,
    Suspended,
    Disconnected
}
