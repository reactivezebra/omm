package com.igmi.om.common;

public enum OrderAim {

    NEW,
    MODIFY,
    DISCONNECT
}
