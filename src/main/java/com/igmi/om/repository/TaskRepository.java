package com.igmi.om.repository;

import com.igmi.om.domain.task.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TaskRepository extends JpaRepository<Task, Long> {

    @Query("SELECT t FROM tasks t WHERE t.isTemplate=true")
    public List<Task> findAllTaskTemplates();
}
