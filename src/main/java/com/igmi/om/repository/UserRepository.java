package com.igmi.om.repository;

import com.igmi.om.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT a FROM accounts a WHERE LOWER(a.email) = LOWER(:email)")
    public User findUserByLogin(@Param("email")String email);
}
