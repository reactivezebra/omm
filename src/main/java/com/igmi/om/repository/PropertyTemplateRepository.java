package com.igmi.om.repository;


import com.igmi.om.form.PropertyTemplate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertyTemplateRepository extends JpaRepository<PropertyTemplate, Long> {
}
