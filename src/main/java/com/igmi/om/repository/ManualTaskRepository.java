package com.igmi.om.repository;

import com.igmi.om.domain.task.ManualTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManualTaskRepository extends JpaRepository<ManualTask, Long> {
}
