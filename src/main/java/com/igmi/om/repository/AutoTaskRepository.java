package com.igmi.om.repository;

import com.igmi.om.domain.task.AutoTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AutoTaskRepository extends JpaRepository<AutoTask, Long> {
}
