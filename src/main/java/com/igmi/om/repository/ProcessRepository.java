package com.igmi.om.repository;

import com.igmi.om.domain.process.Process;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessRepository extends JpaRepository<Process, Long> {
}
