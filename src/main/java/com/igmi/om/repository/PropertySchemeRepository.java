package com.igmi.om.repository;

import com.igmi.om.common.PropertyScheme;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PropertySchemeRepository extends JpaRepository<PropertyScheme, Long> {
}
