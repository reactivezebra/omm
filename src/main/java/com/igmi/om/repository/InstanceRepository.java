package com.igmi.om.repository;

import com.igmi.om.domain.instance.Instance;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InstanceRepository extends JpaRepository<Instance, Long> {
}
