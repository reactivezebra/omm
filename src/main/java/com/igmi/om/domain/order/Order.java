package com.igmi.om.domain.order;

import com.igmi.om.common.OrderAim;
import com.igmi.om.common.PropertyScheme;
import com.igmi.om.common.Status;
import com.igmi.om.domain.instance.Instance;
import com.igmi.om.domain.process.Process;
import com.igmi.om.form.PropertyTemplate;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;
    private String customer;
    private Date startDate = new Date();
    private Date expectedEndDate;
    private Date endDate;

    @OneToOne(cascade = CascadeType.ALL)
    private Process process;

    @OneToOne(cascade = CascadeType.ALL)
    private Instance instance;

//    @OneToMany(fetch = FetchType.EAGER)
//    private List<Order> relatedOrders;

    @Enumerated(EnumType.STRING)
    private OrderAim orderAim;

    @OneToOne
    private PropertyTemplate template;

    @Enumerated(EnumType.STRING)
    private Status status;

    private boolean isPONRpassed;

    @ElementCollection(fetch = FetchType.EAGER)
    private Map<String, String> parameters = new HashMap<String, String>();

    public Order(String name, Date startDate, Process process, Instance instance, OrderAim orderAim, PropertyTemplate template) {
        this.name = name;
        this.startDate = startDate;
        this.process = process;
        this.instance = instance;
        this.orderAim = orderAim;
        setTemplate(template);
    }

    public Order() {
    }

    public Order(String name, PropertyTemplate template, OrderAim orderAim, Instance instance, Process process, Map<String, String> parameters) {
        this.parameters = parameters;
        setTemplate(template);
        this.orderAim = orderAim;
        this.instance = instance;
        this.process = process;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getExpectedEndDate() {
        return expectedEndDate;
    }

    public void setExpectedEndDate(Date expectedEndDate) {
        this.expectedEndDate = expectedEndDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public Instance getInstance() {
        return instance;
    }

    public void setInstance(Instance instance) {
        this.instance = instance;
    }

//    public Collection<Order> getRelatedOrders() {
//        return relatedOrders;
//    }
//
//    public void setRelatedOrders(List<Order> relatedOrders) {
//        this.relatedOrders = relatedOrders;
//    }

    public OrderAim getOrderAim() {
        return orderAim;
    }

    public void setOrderAim(OrderAim orderAim) {
        this.orderAim = orderAim;
    }

    public PropertyTemplate getTemplate() {
        return template;
    }

    public void setTemplate(PropertyTemplate template) {
        this.template = template;
        for (PropertyScheme scheme : template.getItems()) {
            this.parameters.put(scheme.getPropertyName(), "");
        }
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public boolean getIsPONRpassed() {
        return isPONRpassed;
    }

    public void setIsPONRpassed(boolean isPONRpassed) {
        this.isPONRpassed = isPONRpassed;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public void update(Order order) {
        this.name = order.name;
        //this.template = order.template;
        this.orderAim = order.orderAim;
        this.customer = order.customer;

        this.parameters = order.parameters;
    }

    public String getTemplateName() {
        return template != null ? template.getSchemeName() : "";
    }
}
