package com.igmi.om.domain.process;

import com.igmi.om.common.Status;
import com.igmi.om.domain.task.Task;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "processes")
public class Process {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @Enumerated(EnumType.STRING)
    private Status status;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Task> tasks = new ArrayList<Task>();

    public Process(String name, List<Task> tasks) {
        this.name = name;
        this.tasks = tasks;
    }

    public Process(String name) {
        this.name = name;
    }

    public Process() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public void addTask(Task task) {
        tasks.add(task);
    }

}
