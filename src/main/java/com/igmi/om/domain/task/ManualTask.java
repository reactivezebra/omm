package com.igmi.om.domain.task;

import com.igmi.om.domain.User;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

@Entity(name = "manual_tasks")
@DiscriminatorValue("manual")
public class ManualTask extends Task {

    private String taskUrl;
    private String taskJob;

    @OneToOne(cascade = CascadeType.ALL)
    private User assignment;

    public ManualTask(String name, Long duration, User assignment, String job, String url) {
        this.name = name;
        this.duration = duration;
        this.assignment = assignment;
        this.taskJob = job;
        this.taskUrl = url;
    }

    public ManualTask(String name, Long duration, User assignment, String job) {
        this(name, duration, assignment, job, null);
    }

    public ManualTask() {
    }

    public String getTaskUrl() {
        return taskUrl;
    }

    public void setTaskUrl(String task_url) {
        this.taskUrl = task_url;
    }

    public String getTaskJob() {
        return taskJob;
    }

    public void setTaskJob(String task_job) {
        this.taskJob = task_job;
    }

    public User getAssignment() {
        return assignment;
    }

    public void setAssignment(User assignment) {
        this.assignment = assignment;
    }

    @Override
    public void update(Task task) {
        ManualTask tempTask = (ManualTask) task;
        this.name = tempTask.name;
        this.duration = tempTask.duration;
        //this.assignment = tempTask.assignment;
        this.taskJob = tempTask.taskJob;
        this.taskUrl = tempTask.taskUrl;
        this.index = tempTask.index;
    }

    @Override
    public Task clone() {
        ManualTask newTask = new ManualTask();
        newTask.setName(this.getName());
        newTask.setDescription(this.getDescription());
        newTask.setDuration(this.getDuration());
        newTask.setTaskJob(this.getTaskJob());
        newTask.setTaskUrl(this.getTaskUrl());
        newTask.isTemplate = false;
        return newTask;
    }
}
