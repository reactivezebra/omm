package com.igmi.om.domain.task;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "auto_tasks")
@DiscriminatorValue("auto")
public class AutoTask extends Task {

    private String className;

    public AutoTask(String name, String class_name, Long duration) {
        this.name = name;
        this.className = class_name;
        this.duration = duration;
    }

    public AutoTask() {
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String class_name) {
        this.className = class_name;
    }

    public void update(Task task) {
        AutoTask tempTask = (AutoTask) task;
        this.name = tempTask.name;
        this.className = tempTask.className;
        this.index = tempTask.index;
    }

    @Override
    public Task clone() {
        AutoTask newTask = new AutoTask();
        newTask.setName(this.getName());
        newTask.setDescription(this.getDescription());
        newTask.setClassName(this.getClassName());
        newTask.isTemplate = false;
        return newTask;
    }
}
