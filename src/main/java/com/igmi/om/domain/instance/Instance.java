package com.igmi.om.domain.instance;

import com.igmi.om.common.PropertyScheme;
import com.igmi.om.common.Status;
import com.igmi.om.domain.order.Order;
import com.igmi.om.form.PropertyTemplate;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity(name = "instances")
public class Instance {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String description;
    @OneToOne(cascade = CascadeType.ALL)
    private PropertyTemplate template;
    private String customer;

    @Enumerated(EnumType.STRING)
    private Status status;

    @ElementCollection
    private Map<String, String> parameters = new HashMap<String, String>();

    public Instance(String name, PropertyTemplate template, String customer) {
        this.name = name;
        this.template = template;
        this.customer = customer;
    }

    public Instance(String name, PropertyTemplate template, String customer, Map<String, String> parameters) {
        this.name = name;
        this.template = template;
        this.customer = customer;
        this.parameters = parameters;
    }

    public Instance() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PropertyTemplate getTemplate() {
        return template;
    }

    public void setTemplate(PropertyTemplate template) {
        this.template = template;

        for (PropertyScheme param : template.getItems()) {
            this.parameters.put(param.getPropertyName(), "");
        }
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public void update(Instance instance) {
        this.name = instance.name;
        this.template = instance.template;
        this.customer = instance.customer;
        this.description = instance.description;
        this.status = instance.status;
        this.parameters = instance.parameters;

    }
}
