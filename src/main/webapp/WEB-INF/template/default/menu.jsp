<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="menu">
    Menu
    <ul class="nav nav-tabs nav-stacked">
        <li>
            <spring:url value="/home" var="homeUrl" htmlEscape="true"/>
            <a href="${homeUrl}">Home</a>
        </li>
        <li>
            <spring:url value="/users" var="usersUrl" htmlEscape="true"/>
            <a href="${usersUrl}">Users</a>
        </li>
        <li>
            <spring:url value="/orders" var="ordersUrl" htmlEscape="true"/>
            <a href="${ordersUrl}">Orders</a>
        </li>
        <li>
            <spring:url value="/instances" var="instancesUrl" htmlEscape="true"/>
            <a href="${instancesUrl}">Instances</a>
        </li>
        <li>
            <spring:url value="/processes" var="processesUrl" htmlEscape="true"/>
            <a href="${processesUrl}">Processes</a>
        </li>
        <li>
            <spring:url value="/tasks" var="tasksUrl" htmlEscape="true"/>
            <a href="${tasksUrl}">Tasks</a>
        </li>
        <li>
            <spring:url value="/template/properties" var="templatesUrl" htmlEscape="true"/>
            <a href="${templatesUrl}">Property Templates</a>
        </li>
        <li>
            <spring:url value="/about" var="aboutUrl" htmlEscape="true"/>
            <a href="${aboutUrl}">About</a>
        </li>
    </ul>
</div>