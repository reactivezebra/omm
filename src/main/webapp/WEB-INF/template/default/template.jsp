<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title><tiles:insertAttribute name="title" ignore="true"/></title>
    <link rel="stylesheet" href="resources/css/bootstrap.min.css">

    <script src="resources/js/bootstrap.min.js"></script>

    <style media="screen" type="text/css">
        body {
            margin: 0px;
            padding: 0px;
            height: 100%;
            overflow: hidden;
        }

        .page {
            min-height: 100%;
            position: relative;
        }

        .header {
            padding: 10px;
            width: 100%;
            text-align: center;
        }

        .content {
            padding: 10px;
            padding-bottom: 20px; /* Height of the footer element */
            overflow: hidden;
        }

        .menu {
            padding: 50px 10px 0px 10px;
            width: 200px;
            float: left;
        }

        .body {
            margin: 50px 10px 0px 250px;
        }

        .footer {
            clear: both;
            position: absolute;
            bottom: 0;
            left: 0;
            text-align: center;
            width: 100%;
            height: 20px;
        }

    </style>
</head>
<body>
<div class="page">
    <tiles:insertAttribute name="header"/>
    <div class="content">
        <tiles:insertAttribute name="navigation_bar"/>
        <tiles:insertAttribute name="content"/>
    </div>
    <hr>
    <tiles:insertAttribute name="footer"/>
</div>
</body>
</html>