<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div>
    <c:if test="${!empty instances}">
        <h3>Instances</h3>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Template</th>
                <th>Customer</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${instances}" var="instance">
                <tr>
                    <td><a class="btn btn-link" href="/instance/${instance.id}">${instance.name}</a></td>
                    <td>${instance.template}</td>
                    <td>${instance.customer}</td>
                    <td>${instance.status}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>