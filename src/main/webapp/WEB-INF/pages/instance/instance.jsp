<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="edit/${instance.id}" commandName="instance">
            <div>
                <form:label path="name">Name:</form:label>

                <div>
                    <form:input path="name"/>
                </div>
            </div>

            <div>
                <form:label path="description">Description:</form:label>

                <div>
                    <form:input path="description"/>
                </div>
            </div>

            <div>
                <form:label path="template">Template:</form:label>

                <div>
                    <form:input path="template"/>
                </div>
            </div>

            <div>
                <form:label path="customer">Customer:</form:label>

                <div>
                    <form:input path="customer"/>
                </div>
            </div>

            <!--TODO: Parameters!! -->

            <div>
                <div>
                    <input type="submit" value="Save"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>