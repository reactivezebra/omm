<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <c:if test="${!empty propertyForm}">
                <h3>Property templates</h3>
                Name: ${propertyForm.schemeName}<br>
                Description : ${propertyForm.schemeDescription}
                <br>
                <h4>Properties</h4>
                <c:forEach items="${propertyForm.items}" var="propertyScheme">
                    <table>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Type</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>${propertyScheme.propertyName}</td>
                            <td>${propertyScheme.propertyType}</td>
                        </tr>
                        </tbody>
                    </table>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>