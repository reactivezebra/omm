<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<script type="text/javascript"
        src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#paramsTemplate").validate();
    });
    var i = 0;
    function appendRow() {
        $('#properties tr:last')
                .after(
                        '<tr><td><input class="required" type="text" name="items[' + i + '].propertyName" placeholder="Property name"></td><td><select name="items[' + i + '].propertyType" name="propertyType"><option value="STRING">String</option><option value="NUMBER">Number</option></select></td></tr>');
        i++;
    }
    ;
    function removeRow() {
        if ($('#properties tr').length > 1) {
            $('#properties tr:last').remove();
            i--;
        }
    }
    ;
</script>
<div class="span9">
    <form:form method="post" id="paramsTemplate"
               action="createproperty" modelAttribute="paramsTemplate">
        <fieldset>
            <legend>Add category</legend>
            <input class="required" type="text" name="schemeName"
                   placeholder="Scheme name"> <br/>
            <textarea class="required" rows="3" name="schemeDescription"
                      placeholder="Description"></textarea>
            <br/>
            <table class="table table-bordered table-hover" id="properties"
                   style="width: auto;">
                <thead>
                <tr>
                    <th>Property</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody name="paramsTemplate" id="paramsTemplate">
                </tbody>
            </table>
            <button type="button" class="btn" onClick="appendRow();">Add property</button>
            <button type="button" class="btn" onClick="removeRow();">Remove
                last property
            </button>
            <button type="submit" class="btn btn-success">Create</button>
        </fieldset>
    </form:form>
</div>