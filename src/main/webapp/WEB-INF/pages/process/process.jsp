<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <c:if test="${!empty process}">
                <h3>${process.name}</h3>
                Name: ${process.name}<br>
                Status : ${process.status}
                <br>
                <h4>Tasks</h4>
                <c:forEach items="${process.tasks}" var="task">
                    <table>
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Order</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>${task.name}</td>
                            <td>${task.index}</td>
                        </tr>
                        </tbody>
                    </table>
                </c:forEach>
            </c:if>
        </div>
    </div>
</div>