<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div>
    <c:if test="${!empty processes}">
        <h3>Processes</h3>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${processes}" var="process">
                <tr>
                    <td><a class="btn btn-link" href="/process/${process.id}">${process.name}</a></td>
                    <td>${process.status}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>