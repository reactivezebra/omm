<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<script type="text/javascript"
        src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#processForm").validate();
    });
    var i = 0;
    function appendRow() {
        $('#properties tr:last')
                .after(
                        '<tr><td>' +
                        '<select name="tasks[' + i + '].taskId" name="taskId">' +
                        '<c:forEach items="${taskList}" var="task"><option value="${task.id}">${task.name}</option></c:forEach>' +
                        '</select></td>' +
                        '<td><input class="required" type="text" name="tasks[' + i + '].index" placeholder="Index"></td>' +
                        '</tr>');
        i++;
    }
    ;
    function removeRow() {
        if ($('#properties tr').length > 1) {
            $('#properties tr:last').remove();
            i--;
        }
    }
    ;
</script>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="add" id="processForm" modelAttribute="processForm">
            <div>
                <form:label path="name">Name:</form:label>
                <form:input path="name"/>
            </div>

            <table id="properties">
                <thead>
                <tr>
                    <th>Task</th>
                    <th>Index</th>
                </tr>
                </thead>
                <tbody name="processForm" id="processForm">
                </tbody>
            </table>
            <div>
                <button type="button" class="btn" onClick="appendRow();">Add property</button>
                <button type="button" class="btn" onClick="removeRow();">Remove last property
                </button>
                <button type="submit" class="btn btn-success">Create</button>
                </form:form>
            </div>
        </div>
    </div>
</div>
