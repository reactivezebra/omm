<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="add" commandName="orderForm">
            <div>
                <form:label path="name">Name:</form:label>

                <div>
                    <form:input path="name"/>
                </div>
            </div>
            <div>
                <form:label path="description">Description:</form:label>

                <div>
                    <form:input path="description"/>
                </div>
            </div>
            <div>
                <form:label path="customer">Customer:</form:label>

                <div>
                    <form:input path="customer"/>
                </div>
            </div>
            <div>
                <form:label path="orderAim">Order Aim:</form:label>
                <div>
                    <select name="orderAim">
                        <option value="NEW">New</option>
                        <option value="MODIFY">Modify</option>
                        <option value="DISCONNECT">Disconnect</option>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <form:label path="template">Template:</form:label>
                    <select name="templateId">
                        <c:forEach items="${templateList}" var="template">
                            <option value="${template.id}">${template.schemeName}</option>
                        </c:forEach>
                    </select>
                </div>
            </div>
            <div>
                <div>
                    <input type="submit" value="Add Order"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>