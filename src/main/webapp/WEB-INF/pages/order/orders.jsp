<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div>
    <div>
        <c:if test="${!empty orders}">
            <h3>Orders</h3>
            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Template</th>
                    <th>Order Aim</th>
                    <th>Status</th>
                    <th>Instance</th>
                    <th>Process</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${orders}" var="order">
                    <tr>
                        <td><a class="btn btn-link" href="/order/${order.id}">${order.name}</a></td>
                        <td>${order.templateName}</td>
                        <td>${order.orderAim}</td>
                        <td>${order.status}</td>
                        <td><a class="btn btn-link" href="/instance/${order.instance.id}">${order.instance.name}</a>
                        </td>
                        <td><a class="btn btn-link" href="/process/${order.process.id}">${order.process.name}</a></td>
                        <td>${order.startDate}</td>
                        <td>${order.endDate}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </c:if>
    </div>
</div>