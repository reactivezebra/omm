<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="edit/${order.id}" commandName="order">
            <div>
                <form:label path="name">Name:</form:label>

                <div>
                    <form:input path="name"/>
                </div>
            </div>

            <div>
                <form:label path="description">Description:</form:label>

                <div>
                    <form:input path="description"/>
                </div>
            </div>

            <div>
                <form:label path="customer">Customer:</form:label>

                <div>
                    <form:input path="customer"/>
                </div>
            </div>

            <div>
                <form:label path="startDate">Start Date:</form:label>

                <div>
                    <form:input path="startDate"/>
                </div>
            </div>

            <div>
                <form:label path="expectedEndDate">Expected End Date:</form:label>

                <div>
                    <form:input path="expectedEndDate"/>
                </div>
            </div>

            <div>
                <form:label path="endDate">End Date:</form:label>

                <div>
                    <form:input path="endDate"/>
                </div>
            </div>

            <div>
                <form:label path="orderAim">Order Aim:</form:label>

                <div>
                    <form:input path="orderAim"/>
                </div>
            </div>

            <div>
                <form:label path="templateName">Template: <a
                        href="/template/property/${order.template.id}">${order.templateName}</a></form:label>
            </div>

            <table>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Value</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${order.parameters}" var="parameter">
                    <tr>
                        <td>${parameter.key}</td>
                        <td><input name="parameters['${parameter.key}']" value="${parameter.value}"/></td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
            <div>
                <div>
                    <input type="submit" value="Save"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>