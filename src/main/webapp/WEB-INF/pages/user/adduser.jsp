<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="add" commandName="user">
            <div>
                <form:label path="firstName">First Name:</form:label>

                <div>
                    <form:input path="firstName"/>
                </div>
            </div>
            <div>
                <form:label path="lastName">Last Name:</form:label>

                <div>
                    <form:input path="lastName"/>
                </div>
            </div>
            <div>
                <form:label path="email">Email:</form:label>

                <div>
                    <form:input path="email"/>
                </div>
            </div>
            <div>
                <form:label path="password">Password:</form:label>

                <div>
                    <form:input path="password"/>
                </div>
            </div>
            <div>
                <form:label path="enabled">Is enabled?:</form:label>

                <div>
                    <form:checkbox path="enabled"/>
                </div>
            </div>
            <div>
                <form:label path="isAdmin">Is admin?:</form:label>

                <div>
                    <form:checkbox path="isAdmin"/>
                </div>
            </div>
            <div>
                <div>
                    <input type="submit" value="Add User"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>