<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <c:if test="${!empty users}">
                <h3>Users</h3>
                <table>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>&nbsp;</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${users}" var="user">
                        <tr>
                            <td><a class="btn btn-link" href="/user/${user.id}">${user.lastName}, ${user.firstName}</a>
                            </td>
                            <td>
                                <form action="delete/${user.id}" method="post"><input class="btn btn-block"
                                                                                      type="submit" value="Delete"/>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>