<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="/task/edit/auto/${task.id}" commandName="task">
            <div>
                <form:label path="name">Name:</form:label>

                <div>
                    <form:input path="name"/>
                </div>
            </div>
            <div>
                <form:label path="duration">Duration:</form:label>

                <div>
                    <form:input path="duration"/>
                </div>
            </div>
            <div>
                <form:label path="className">Class name:</form:label>

                <div>
                    <form:input path="className"/>
                </div>
            </div>
            <div>
                <form:label path="isTemplate">Is Template?:</form:label>

                <div>
                    <form:input path="isTemplate"/>
                </div>
            </div>
            <div>
                <form:label path="index">Index:</form:label>
                <div>
                    <form:input path="index"/>
                </div>
            </div>
            <div>
                <div>
                    <input type="submit" value="Save"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
