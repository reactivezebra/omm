<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div>
    <c:if test="${!empty tasks}">
        <h3>Tasks</h3>
        <table>
            <thead>
            <tr>
                <th>Name</th>
                <th>Duration</th>
                <th>Start Date</th>
                <th>End Date</th>
                <th>Status</th>
                <th>Index</th>
                <th>Is Template?</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${tasks}" var="task">
                <tr>
                    <td><a class="btn btn-link" href="/task/${task.id}">${task.name}</a></td>
                    <td>${task.duration}</td>
                    <td>${task.startDate}</td>
                    <td>${task.endDate}</td>
                    <td>${task.status}</td>
                    <td>${task.index}</td>
                    <td>${task.isTemplate}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </c:if>
</div>