<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<div class="body">
    <div>
        <div>
            <form:form method="post" action="manual" commandName="task">
            <div>
                <form:label path="name">Name:</form:label>

                <div>
                    <form:input path="name"/>
                </div>
            </div>
            <div>
                <form:label path="duration">Duration:</form:label>

                <div>
                    <form:input path="duration"/>
                </div>
            </div>
            <div>
                <form:label path="taskJob">Target:</form:label>

                <div>
                    <form:input path="taskJob"/>
                </div>
            </div>
            <div>
                <form:label path="taskUrl">Target URL:</form:label>

                <div>
                    <form:input path="taskUrl"/>
                </div>
            </div>
            <div>
                <form:label path="index">Index:</form:label>
                <div>
                    <form:input path="index"/>
                </div>
            </div>
            <div>
                <div>
                    <input type="submit" value="Add Task"/>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>
